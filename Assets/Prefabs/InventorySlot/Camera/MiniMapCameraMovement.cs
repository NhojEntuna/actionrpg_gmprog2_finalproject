﻿using UnityEngine;
using System.Collections;

public class MiniMapCameraMovement : MonoBehaviour 
{
    public Transform m_Target;
    public float     m_Smoothing;

    Vector3 offset;

    void Start()
    {
        offset = this.transform.position - m_Target.position;
    }

    void Update()
    {
        Vector3 targetCamPos = m_Target.position + offset;
        this.transform.position = Vector3.Lerp(this.transform.position, targetCamPos, m_Smoothing * Time.deltaTime);
    }
}
