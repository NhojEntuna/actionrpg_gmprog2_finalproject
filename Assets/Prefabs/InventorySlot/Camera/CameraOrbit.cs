﻿using UnityEngine;
using System.Collections;

public class CameraOrbit : PauseManager 
{
    public Transform m_Target;
    public float     m_distance = 10.0f;

    public float m_xSpeed = 0.0f;
    public float m_ySpeed = 0.0f;
    public float m_yMinLimit = 0.0f;
    public float m_yMaxLimit = 0.0f;

    private float m_xRot = 0.0f;
    private float m_yRot = 0.0f;

    Character_Stats player;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Character_Stats>();
        Vector3 angle = transform.eulerAngles;
        m_xRot = angle.x;
        m_yRot = angle.y;

        if (GetComponent<Rigidbody>())
        {
            GetComponent<Rigidbody>().freezeRotation = true;
        }
    }

    public override void MoveLateUpdate()
    {
        if (m_Target)
        {
            m_xRot += Input.GetAxis("Mouse X") * m_xSpeed * Time.deltaTime;
            m_yRot -= Input.GetAxis("Mouse Y") * m_ySpeed * Time.deltaTime;

            m_yRot = ClampAngle(m_yRot, m_yMinLimit, m_yMaxLimit);

            Quaternion targetRot = Quaternion.Euler(m_yRot, m_xRot, 0.0f);
            Vector3 targetPos = targetRot * new Vector3(0.0f, 0.0f, -m_distance) + m_Target.position;

          if (player.getPlayerHealth() > 0)
          {
            this.transform.rotation = targetRot;
            this.transform.position = targetPos;
            this.transform.position = new Vector3(targetPos.x, this.transform.position.y, targetPos.z);
          }
        }
        else
            return;

        base.MoveLateUpdate();
    }

    static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
        {
            angle += 360;
        }
        if (angle > 360)
        {
            angle -= 360;
        }

        return Mathf.Clamp(angle, min, max);
    }
}
