﻿using UnityEngine;
using System.Collections;

public class CameraMovement_MainMenu : MonoBehaviour 
{
    public Transform m_upBoundary;
    public Transform m_downBoundary;
    public float m_Smoothing;

    private bool m_Move = true;
    void Update()
    {
        if (m_Move)
        {
            this.transform.position = Vector3.Lerp(this.transform.position, m_upBoundary.position, m_Smoothing * Time.deltaTime);
        }
        else if (!m_Move)
            this.transform.position = Vector3.Lerp(this.transform.position, m_downBoundary.position, m_Smoothing * Time.deltaTime);

        if (Vector3.Distance(this.transform.position, m_upBoundary.position) <= 0.1f)
        {
            m_Move = false;
        }
        else if (Vector3.Distance(this.transform.position, m_downBoundary.position) <= 0.1f)
            m_Move = true;
    }
}
