﻿using UnityEngine;
using System.Collections;
public class MainMenuManager : MonoBehaviour 
{
    public GameObject CreditsPanel;
    public GameObject HowToPanel;
    private bool creditShow = false;
    private bool howShow = false;

    void Update()
    {
        if (creditShow)
            CreditsPanel.SetActive(true);
        else
            CreditsPanel.SetActive(false);

        if (howShow)
            HowToPanel.SetActive(true);
        else
            HowToPanel.SetActive(false);
    }
	public void startGame(string level)
	{
		Application.LoadLevel(level);
	}

	public void hotToPlay()
	{
        howShow = !howShow;
	}

	public void creditScreen()
	{
        creditShow = !creditShow;
	}
}
