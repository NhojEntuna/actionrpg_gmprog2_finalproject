﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Item : PauseManager 
{
	public int        Cost;
	public string     ItemName;
    public int        ItemID;
	public Sprite     Image;
    public string     ItemDescription;
	public int        ItemQuantity;
}
