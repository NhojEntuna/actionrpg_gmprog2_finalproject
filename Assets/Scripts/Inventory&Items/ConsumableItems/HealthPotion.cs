﻿using UnityEngine;
using System.Collections;

public class HealthPotion : ConsumableItem 
{
    public int m_HealthIncrease;
    public override void excuteItem()
    {
        this.RaiseEventGlobal<PlayerHealthEvent>(new PlayerHealthEvent(m_HealthIncrease));
        base.excuteItem();
    }
}
