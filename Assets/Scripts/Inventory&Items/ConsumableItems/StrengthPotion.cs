﻿using UnityEngine;
using System.Collections;

public class StrengthPotion : ConsumableItem 
{
	public int StrengthIncrease;
    public override void excuteItem()
    {
        this.RaiseEventGlobal<PlayerStrengthEvent>(new PlayerStrengthEvent 
        {
            strengthIncrease = StrengthIncrease
        });
        base.excuteItem();
    }
}
