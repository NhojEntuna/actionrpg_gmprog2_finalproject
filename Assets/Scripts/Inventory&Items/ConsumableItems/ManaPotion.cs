﻿using UnityEngine;
using System.Collections;

public class ManaPotion : ConsumableItem
{
    public int m_ManaIncrease;
    public override void excuteItem()
    {
        this.RaiseEventGlobal<PlayerManaEvent>(new PlayerManaEvent
            {
                manaIncrease = m_ManaIncrease
            });
        base.excuteItem();
    }
}
