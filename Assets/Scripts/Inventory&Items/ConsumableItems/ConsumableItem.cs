﻿using UnityEngine;
using System.Collections;

public class ConsumableItem : Item 
{
    public virtual void excuteItem()
    { }

	public override void Move()
	{
		this.transform.Rotate(0.0f, 0f, 180.0f * Time.deltaTime);
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Destroy(this.gameObject.GetComponent<MeshRenderer>());
            Destroy(this.gameObject.GetComponent<Collider>());
            ConsumableItem newConsumable = this.gameObject.GetComponent<ConsumableItem>();
            other.gameObject.GetComponent<Character_Inventory>().addConsumableItem(newConsumable);
        }
    }
}
