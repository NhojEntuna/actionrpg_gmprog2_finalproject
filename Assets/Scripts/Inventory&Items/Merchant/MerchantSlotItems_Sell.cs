﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MerchantSlotItems_Sell : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler 
{
    ConsumableItem item;
    Image itemImage;
    MerchantPanel_Sell consumableInventory; // The Panel of inventory

    public int slotNumber;
    public Text itemQuantity;

    void Start()
    {
        consumableInventory = GameObject.FindGameObjectWithTag("SellPanel").GetComponent<MerchantPanel_Sell>();
        itemImage = gameObject.transform.GetChild(0).GetComponent<Image>();
    }

    void Update()
    {
        if (consumableInventory.m_consumableItems[slotNumber].ItemName != null)
        {
            item = consumableInventory.m_consumableItems[slotNumber];
            itemImage.enabled = true;
            itemImage.sprite = consumableInventory.m_consumableItems[slotNumber].Image;
            itemQuantity.text = "" + consumableInventory.m_consumableItems[slotNumber].ItemQuantity;
        }
        else
            itemImage.enabled = false;

        if (consumableInventory.m_consumableItems[slotNumber].ItemQuantity == 0)
        {
            this.RaiseEventGlobal<DestroyCellEvent>(new DestroyCellEvent
            {
                slotID = slotNumber
            });
        }
    }

    public void OnPointerDown(PointerEventData data)
    {
        if (consumableInventory.m_consumableItems[slotNumber].ItemName != null && consumableInventory.m_consumableItems[slotNumber].ItemQuantity > 0)
        {
            GetPlayerInventoryEvent inventory = new GetPlayerInventoryEvent();
            this.RaiseEventGlobal<GetPlayerInventoryEvent>(inventory);
            if  (inventory.charInventory.ConsumableInventory[slotNumber].ItemQuantity > 0)
            {
                this.RaiseEventGlobal<GoldEvent>(new GoldEvent(consumableInventory.m_consumableItems[slotNumber].Cost));
                inventory.charInventory.removeConsumable(slotNumber);
            }
        }
    }
    public void OnPointerEnter(PointerEventData data)
    {
        if (consumableInventory.m_consumableItems[slotNumber] != null)
        this.RaiseEventGlobal<ItemDescriptionEvent>(new ItemDescriptionEvent
        {
            ItemName = consumableInventory.m_consumableItems[slotNumber].ItemName,
            ItemDescription = consumableInventory.m_consumableItems[slotNumber].ItemDescription + " ----->Item Value: " + consumableInventory.m_consumableItems[slotNumber].Cost + " Gold"
        });
    }
    public void OnPointerExit(PointerEventData data)
    {
        this.RaiseEventGlobal<ItemDescriptionEvent>(new ItemDescriptionEvent
        {
            ItemName = "",
            ItemDescription = ""
        });
    }

}
