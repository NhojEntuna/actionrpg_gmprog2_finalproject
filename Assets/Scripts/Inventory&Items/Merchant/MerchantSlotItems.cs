﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class MerchantSlotItems : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler 
{
    public int     slotNumber;
    ConsumableItem item;
    Image          itemImage;
    MerchantPanel  Items;
    void Start()
    {
        Items = GameObject.FindGameObjectWithTag("Merchant").GetComponent<MerchantPanel>();
        itemImage = gameObject.transform.GetChild(0).GetComponent<Image>();
    }

    void Update()
    {
        if (Items.MerchantItems[slotNumber].ItemName != null)
        {
            item = Items.MerchantItems[slotNumber];
            itemImage.enabled = true;
            itemImage.sprite = Items.MerchantItems[slotNumber].Image;
        }
        else
            itemImage.enabled = false;
    }

    public void OnPointerDown(PointerEventData data)
    {
        GetPlayerGoldEvent gold = new GetPlayerGoldEvent();
        this.RaiseEventGlobal<GetPlayerGoldEvent>(gold);
        int playerGold = gold.charGold.getGold();
        if (playerGold >= Items.MerchantItems[slotNumber].Cost)
        {
            ConsumableItem newAcquiredItem = Items.MerchantItems[slotNumber];
            GetPlayerInventoryEvent inventory = new GetPlayerInventoryEvent();
            this.RaiseEventGlobal<GetPlayerInventoryEvent>(inventory);
            inventory.charInventory.addConsumableItem(newAcquiredItem);;
            this.RaiseEventGlobal<GoldEvent>(new GoldEvent(-Items.MerchantItems[slotNumber].Cost));
        }
		else if(playerGold < Items.MerchantItems[slotNumber].Cost)
        {
            this.RaiseEventGlobal<ItemDescriptionEvent>(new ItemDescriptionEvent
            {
                ItemName = "",
                ItemDescription = "Not Enough Gold!"
            });
        }
        
    }
    public void OnPointerEnter(PointerEventData data)
    {
        if (Items.MerchantItems[slotNumber].ItemName != null)
		{
            this.RaiseEventGlobal<ItemDescriptionEvent>(new ItemDescriptionEvent
            {
                ItemName = Items.MerchantItems[slotNumber].ItemName,
                ItemDescription = Items.MerchantItems[slotNumber].ItemDescription + "  Cost: " + Items.MerchantItems[slotNumber].Cost
            });
		}
    }

    public void OnPointerExit(PointerEventData data)
    {
        this.RaiseEventGlobal<ItemDescriptionEvent>(new ItemDescriptionEvent
        {
            ItemName = "",
            ItemDescription = ""
        });
    }
}
