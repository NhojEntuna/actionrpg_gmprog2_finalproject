﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System;
public class MerchantPanel : MonoBehaviour 
{
	public List<GameObject> MerchantSlots = new List<GameObject>();
	public List<ConsumableItem> MerchantItems = new List<ConsumableItem>();
	public GameObject m_InventorySlot;
    public Text PlayerGoldText;
	public int rows;
	public int columns;

	public GameObject merchantItems;

    MerchantItems items;
	int slotAmount = 0;
	float xPos = -165f;
	float yPos = -59.2f;


	void Awake () 
	{
        items = merchantItems.GetComponent<MerchantItems>();	
	}
    void OnEnable()
    {
        float xPos = -165f;
        float yPos = -59.2f;
        slotAmount = 0;

        if (items.Items.Count != 0)
        {
            for (int i = 0; i < items.Items.Count; i++)
            {
                GameObject slot = Instantiate(m_InventorySlot) as GameObject;
                slot.GetComponent<MerchantSlotItems>().slotNumber = slotAmount;
                MerchantSlots.Add(slot); // for slots
                MerchantItems.Add(new ConsumableItem()); // consumable items
                slot.transform.parent = this.gameObject.transform; // this will be a child of this object
                slot.GetComponent<RectTransform>().localPosition = new Vector3(xPos, yPos, 0.0f);
                xPos += 110;
                if (slotAmount == columns - 1)
                {
                    xPos = -165f;
                    yPos = yPos - 80;
                }
                slotAmount++;
                
            }
        }

        foreach (ConsumableItem item in items.Items)
        {
            addConsumable(item.ItemID);
        }
    }
    void OnDisable()
    {
        for (int i = 0; i < MerchantSlots.Count; i++)
        {
            Destroy(MerchantSlots[i]);
        }

        MerchantSlots.Clear();
        MerchantItems.Clear();
    }
    void Update()
    {
        GetPlayerGoldEvent gold = new GetPlayerGoldEvent();
        this.RaiseEventGlobal<GetPlayerGoldEvent>(gold);
        PlayerGoldText.text = "Player Gold: " + gold.charGold.getGold();
    }

    void addConsumable(int id)
    {
        var query = (from item in items.Items where item.ItemID == id select item).SingleOrDefault();
        if (query != null)
        {
            addItemEmptySlot((ConsumableItem)query);
        }
    }

    void addItemEmptySlot(ConsumableItem item)
    {
        for (int i = 0; i < MerchantItems.Count; i++)
        {
            if (MerchantItems[i].ItemName == null)
            {
                MerchantItems[i] = item;
                break;
            }
        }
    }


    // UI Functions
    public void exitPanel()
    {
        this.gameObject.SetActive(false);
    }
}
