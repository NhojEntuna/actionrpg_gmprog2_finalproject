﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MerchantShop : MonoBehaviour
{
    public GameObject MerchantBuySell;
    public GameObject MerchantBuy;
    public GameObject MerchantSell;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            MerchantBuySell.SetActive(true);
        }
    }

    public void exitPanel()
    {
        MerchantBuySell.SetActive(false);
    }
    public void buyItem()
    {
        MerchantBuySell.SetActive(false);
        MerchantBuy.SetActive(true);
    }
    public void sellItem()
    {
        MerchantBuySell.SetActive(false);
        MerchantSell.SetActive(true);
    }
}
