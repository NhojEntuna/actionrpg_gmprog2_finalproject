﻿using UnityEngine;
using System.Collections;

public class EquipmentItem : Item
{
    protected Character_Stats player;
    public virtual void addWeaponEffect() { }
    public virtual void executeWeapon() { }
    public virtual void revertWeaponEffect() { }

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Character_Stats>();
    }
    void OnEnable()
    {
        addWeaponEffect();
    }
    void OnDisable() 
    {
        revertWeaponEffect();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Destroy(this.gameObject.GetComponent<MeshRenderer>());
            Destroy(this.gameObject.GetComponent<Collider>());
            EquipmentItem newEquipment = this.gameObject.GetComponent<EquipmentItem>();
            this.RaiseEventGlobal<AddEquipmentEvent>(new AddEquipmentEvent(newEquipment));
        }

    }
}
