﻿using UnityEngine;
using System.Collections;

public class MeleeWeapon : EquipmentItem 
{
	public Range Damage;
	public int MeleeStrengthIncrease;
	int totalDamage;
    public override void addWeaponEffect()
    {
        player.addstrength(MeleeStrengthIncrease);
        base.addWeaponEffect();
    }

    public override void revertWeaponEffect()
    {
        this.RaiseEventGlobal<PlayerStrengthEvent>(new PlayerStrengthEvent
        {
            strengthIncrease = -MeleeStrengthIncrease
        });
        base.revertWeaponEffect();
    }

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.GetComponent<Enemy>() != null)
		{
            GetPlayerStatsEvent stats = new GetPlayerStatsEvent();
            this.RaiseEventGlobal<GetPlayerStatsEvent>(stats);
            totalDamage = stats.charStats.getStrength() + Random.Range(Damage.Min, Damage.Max);
			other.gameObject.SendMessage("takeDamage", totalDamage);
		}
	}
}
