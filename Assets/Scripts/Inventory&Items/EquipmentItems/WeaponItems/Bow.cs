﻿using UnityEngine;
using System.Collections;

public class Bow : EquipmentItem
{
    public Transform arrowSpawnPoint;
    public GameObject Arrow;
    public int StrengthIncrease;

    private bool fire = true;
    public override void addWeaponEffect()
    {
        this.RaiseEventGlobal<PlayerStrengthEvent>(new PlayerStrengthEvent
        {
            strengthIncrease = StrengthIncrease
        });
        //player.addstrength(StrengthIncrease);
        base.addWeaponEffect();
    }
    public override void revertWeaponEffect()
    {
        this.RaiseEventGlobal<PlayerStrengthEvent>(new PlayerStrengthEvent
        {
            strengthIncrease = -StrengthIncrease
        });
       // player.addStrength(-StrengthIncrease);
        base.revertWeaponEffect();
    }
    public override void executeWeapon()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (fire)
        {
            GameObject arrow = Instantiate(Arrow, arrowSpawnPoint.position, player.transform.rotation) as GameObject;
            Destroy(arrow, 2);
            fire = false;
            StartCoroutine(timer(.5f));
        }
        base.executeWeapon();
    }

    IEnumerator timer(float time)
    {
        yield return new WaitForSeconds(time);
        fire = true;
    }
}
