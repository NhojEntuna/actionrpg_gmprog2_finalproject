﻿using UnityEngine;
using System.Collections;

public class BowArrow_Movement : PauseManager 
{
    public float Speed;
    public Range Damage;
    int totalDamage;
    void OnEnable()
    {
        this.AddEventListenerGlobal<PauseEvent>(OnPause);
    }
    void OnDisable()
    {
        this.RemoveEventListenerGlobal<PauseEvent>(OnPause);
    }
    void OnPause(PauseEvent e)
    {
        isPaused = e.paused;
    }   
    public override void Move()
    {
        this.transform.Translate(Vector3.forward * Time.deltaTime * Speed);
        base.Move();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Enemy>() != null)
        {
            GetPlayerStatsEvent stats = new GetPlayerStatsEvent();
            this.RaiseEventGlobal<GetPlayerStatsEvent>(stats);
            totalDamage = stats.charStats.getStrength() + Random.Range(Damage.Min, Damage.Max);
            other.gameObject.SendMessage("takeDamage", totalDamage);
            Destroy(this.gameObject);
        }
    }
}
