﻿using UnityEngine;
using System.Collections;

public class Bow_Event : PauseManager 
{
    public EquipmentItem bow;
    public override void Move()
    {
        this.transform.Rotate(0.0f, 0f, 180.0f * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            this.RaiseEventGlobal<AddEquipmentEvent>(new AddEquipmentEvent(bow));
            Destroy(this.gameObject);
        }

    }
}
