﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ItemDescriptionEvent : GameEvent
{
    public string ItemName;
    public string ItemDescription;
}

public class ItemInformation : MonoBehaviour 
{
    public Text itemName;
    public Text itemDescription;
    void OnEnable()
    {
        this.AddEventListenerGlobal<ItemDescriptionEvent>(displayItemInformation);
    }
    void OnDisable()
    {
        this.RemoveEventListenerGlobal<ItemDescriptionEvent>(displayItemInformation);
    }

    void displayItemInformation(ItemDescriptionEvent e)
    {
        itemName.text = e.ItemName;
        itemDescription.text = e.ItemDescription;
    }
}
