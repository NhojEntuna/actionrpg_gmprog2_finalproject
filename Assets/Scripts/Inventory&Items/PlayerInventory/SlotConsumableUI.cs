﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SlotConsumableUI : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    ConsumableItem item;
    Image          itemImage;

    InventorySystemUI consumableInventory; // The Panel of inventory
    public int slotNumber;
	public Text itemQuantity;

    void Start()
    {
        consumableInventory = GameObject.FindGameObjectWithTag("Inventory").GetComponent<InventorySystemUI>();
        itemImage = gameObject.transform.GetChild(0).GetComponent<Image>();
    }

    void Update()
    {
        if (consumableInventory.m_consumableItems[slotNumber].ItemName != null)
        {
            item = consumableInventory.m_consumableItems[slotNumber]; 
            itemImage.enabled = true;
            itemImage.sprite = consumableInventory.m_consumableItems[slotNumber].Image;
			itemQuantity.text = "" + consumableInventory.m_consumableItems[slotNumber].ItemQuantity;
        }
        else
            itemImage.enabled = false;

		if (consumableInventory.m_consumableItems[slotNumber].ItemQuantity == 0)
		{
            this.RaiseEventGlobal<DestroyCellEvent>(new DestroyCellEvent 
            {
                slotID = slotNumber
            });
		}
    }

    public void OnPointerDown(PointerEventData data)
    {
		if (consumableInventory.m_consumableItems[slotNumber].ItemName != null && consumableInventory.m_consumableItems[slotNumber].ItemQuantity > 0)
		{
			// deduct the quantity and execute the item
			consumableInventory.m_consumableItems[slotNumber].ItemQuantity--; // deduct the quantity;
			consumableInventory.m_consumableItems[slotNumber].excuteItem();
		}
    }
    public void OnPointerEnter(PointerEventData data)
    {
        if (consumableInventory.m_consumableItems[slotNumber].ItemName != null)
            this.RaiseEventGlobal<ItemDescriptionEvent>(new ItemDescriptionEvent 
            {
                ItemName = consumableInventory.m_consumableItems[slotNumber].ItemName,
                ItemDescription = consumableInventory.m_consumableItems[slotNumber].ItemDescription
            });
    }

    public void OnPointerExit(PointerEventData data)
    {
        this.RaiseEventGlobal<ItemDescriptionEvent>(new ItemDescriptionEvent
        {
            ItemName = "",
            ItemDescription = ""
        });
    }

}
