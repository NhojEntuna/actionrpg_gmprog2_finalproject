﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System;

public class DestroyCellEvent : GameEvent
{
    public int slotID;
}

public class InventorySystemUI : MonoBehaviour 
{
    public List<GameObject> m_Slots = new List<GameObject>(); // slots
    public List<ConsumableItem> m_consumableItems = new List<ConsumableItem>(); // consumable inventory
    public GameObject m_InventorySlot;
	public Text PlayerGold;
	private List<GameObject> m_TempSlots = new List<GameObject>(); // temp slots

    float xPos = -165.9f;
    float yPos = 0f;
    int slotAmount = 0;
    bool callOnce = true;
	GameObject slot;
    void OnEnable()
    {
        GetPlayerGoldEvent gold = new GetPlayerGoldEvent();
        this.RaiseEventGlobal<GetPlayerGoldEvent>(gold);
		PlayerGold.text = "Player Gold: " + gold.charGold.getGold();
		float xPos = -165.9f;
		float yPos = 0f;
        slotAmount = 0;

        // Add new cells
        // Raises event to get the inventory of the player
        GetPlayerInventoryEvent playerInventory = new GetPlayerInventoryEvent();
        this.RaiseEventGlobal<GetPlayerInventoryEvent>(playerInventory);
        if (playerInventory.charInventory.ConsumableInventory.Count != 0)
        {
            for (int i = 0; i < playerInventory.charInventory.ConsumableInventory.Count; i++)
            {
                slot = Instantiate(m_InventorySlot) as GameObject;
                slot.GetComponent<SlotConsumableUI>().slotNumber = slotAmount;
                m_Slots.Add(slot); // for slots
                m_TempSlots.Add(slot);
                m_consumableItems.Add(new ConsumableItem()); // consumable items
                slot.transform.parent = this.gameObject.transform; // this will be a child of this object
                slot.GetComponent<RectTransform>().localPosition = new Vector3(xPos, yPos, 0.0f);
                xPos += 110;
                if (slotAmount == 3)
                {
                    xPos = -165.9f;
                    yPos = yPos - 80;
                }
                slotAmount++;
               
            }
        }
        this.AddEventListenerGlobal<DestroyCellEvent>(deleteSlot);
		// adding the character's inventory to the ui
        foreach (ConsumableItem item in playerInventory.charInventory.ConsumableInventory)
		{
			addConsumable(item.ItemID);
		}
        
    }
	void OnDisable()
	{
		for (int i = 0; i < m_TempSlots.Count(); i++)
		{
		  Destroy(m_TempSlots[i]);
		}

		m_Slots.Clear();
		m_consumableItems.Clear();
        this.RemoveEventListenerGlobal<DestroyCellEvent>(deleteSlot);
	}

    void addConsumable(int id)
    {
        GetPlayerInventoryEvent inventory = new GetPlayerInventoryEvent();
        this.RaiseEventGlobal<GetPlayerInventoryEvent>(inventory);
        var query = (from item in inventory.charInventory.ConsumableInventory where item.ItemID == id select item).SingleOrDefault();
        if (query != null) 
		{
            addItemEmptySlot((ConsumableItem)query);
        }
    }

    void addItemEmptySlot(ConsumableItem item)
    {
        for (int i = 0; i < m_consumableItems.Count; i++)
        {
            if (m_consumableItems[i].ItemName == null)
            {
                m_consumableItems[i] = item;
                break;
            }
        }
    }

    void deleteSlot(DestroyCellEvent e)
    {
        Destroy(m_Slots[e.slotID].gameObject);
    }

}
