﻿using UnityEngine;
using System.Collections;

public class CoinReward : PauseManager
{
    public int GoldValue;
    public override void Move()
    {
        this.transform.Rotate(0.0f, 180.0f * Time.deltaTime, 0.0f);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            this.RaiseEventGlobal<GoldEvent>(new GoldEvent(GoldValue));
            Destroy(this.gameObject);
        }
    }
}
