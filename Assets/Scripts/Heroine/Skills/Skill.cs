﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Skill : PauseManager 
{
	public string SkillName;
	public Sprite SkillImage;
	public int    ManaCost;
    public int    SkillID;
    public float SkillDuration;

    public virtual void executeSkill() { }

}
