﻿using UnityEngine;
using System.Collections;


public class HealSkill : Skill 
{
    public Range healRange;
   public override void executeSkill()
    {
        // edit the amount
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        GameObject skill = Instantiate(this.gameObject, player.transform.position, this.transform.rotation) as GameObject;
        Destroy(skill.gameObject, SkillDuration);
        this.RaiseEventGlobal<PlayerHealthEvent>(new PlayerHealthEvent(Random.Range(healRange.Min, healRange.Max)));
 	    base.executeSkill();
    }
}
