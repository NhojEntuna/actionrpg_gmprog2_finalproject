﻿using UnityEngine;
using System.Collections;

public class Firebolt : DamageSkill 
{
	public override void skillMovement () // derived from DamageSkill.cs
	{
		this.transform.Translate(Vector3.forward * Time.deltaTime * Speed);
		base.skillMovement ();
	}
    public override void executeSkill() // derived from Skill.cs
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        GameObject skill = Instantiate(this.gameObject, player.GetComponent<Character_Skills>().skillLocation.position, player.transform.rotation) as GameObject;
        Destroy(skill.gameObject, SkillDuration); //check
        base.executeSkill();
    }
}
