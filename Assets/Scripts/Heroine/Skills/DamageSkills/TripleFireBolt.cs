﻿using UnityEngine;
using System.Collections;

public class TripleFireBolt : DamageSkill 
{
	public override void skillMovement ()
	{
		this.transform.Translate(Vector3.forward * Time.deltaTime * Speed);
		base.skillMovement ();
	}
    public override void executeSkill()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        GameObject skill_1 = Instantiate(this.gameObject, player.GetComponent<Character_Skills>().skillLocation.position, player.transform.localRotation) as GameObject;
        GameObject skill_2 = Instantiate(this.gameObject, player.GetComponent<Character_Skills>().skillLocation.position, player.transform.localRotation) as GameObject;
        GameObject skill_3 = Instantiate(this.gameObject, player.GetComponent<Character_Skills>().skillLocation.position, player.transform.localRotation) as GameObject;
        skill_2.gameObject.transform.transform.Rotate(0.0f, 10.0f, 0.0f);
        skill_3.gameObject.transform.transform.Rotate(0.0f, -10.0f, 0.0f);
        Destroy(skill_1.gameObject, SkillDuration);
        Destroy(skill_2.gameObject, SkillDuration);
        Destroy(skill_3.gameObject, SkillDuration);
        base.executeSkill();
    }
}
