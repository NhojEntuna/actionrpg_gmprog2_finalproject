﻿using UnityEngine;
using System.Collections;

public class DamageSkill : Skill 
{
    public Range Damage;
	public float Speed;
	public virtual void skillMovement(){}
    public override void Move()
	{
		skillMovement();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.GetComponent<Enemy>() != null)
		{
            GetPlayerStatsEvent stats = new GetPlayerStatsEvent();
            this.RaiseEventGlobal<GetPlayerStatsEvent>(stats);
            int magicalAttack = stats.charStats.getMagicalAttack();
            int totalDamage = magicalAttack + Random.Range(Damage.Min, Damage.Max);
			other.gameObject.SendMessage("takeDamage", totalDamage);
			Destroy(this.gameObject);
			Debug.Log("Hit!");
		}
	}
}
