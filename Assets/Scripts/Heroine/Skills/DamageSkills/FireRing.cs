﻿using UnityEngine;
using System.Collections;

public class FireRing : DamageSkill 
{
	public override void skillMovement ()
	{
		//this.transform.localRotation = Quaternion.Euler(0.0f, 180.0f * Time.deltaTime, 0.0f);
		this.transform.Translate(Vector3.forward * Time.deltaTime * Speed);
		base.skillMovement ();
	}

    public override void executeSkill()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        GameObject skill = Instantiate(this.gameObject, player.GetComponent<Character_Skills>().skillLocation.position, player.transform.rotation) as GameObject;
        Destroy(skill.gameObject, SkillDuration);
        base.executeSkill();
    }
}
