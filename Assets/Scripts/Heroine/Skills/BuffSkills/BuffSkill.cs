﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class StatsModifier
{
    public int Strength;
    public int Defense;
    public int MDefense;
    public int MAttack;
}

public class BuffSkill : Skill
{
    public List<StatsModifier> Modifiers = new List<StatsModifier>();
    public bool isActive = false;

    private float timeElapsed;

    void Update()
    {
        //Timer
        if (isActive)
        {
            timeElapsed += Time.deltaTime;
            if (timeElapsed >= SkillDuration)
            {
                this.RaiseEventGlobal<ModifyStatsEvent>(new ModifyStatsEvent
                {
                    strengthModifier = -Modifiers[0].Strength,
                    magicalDefenseModifier = -Modifiers[0].MDefense,
                    defenseModifier = -Modifiers[0].Defense,
                    magicalAttackModifier = -Modifiers[0].MAttack
                });
                isActive = false;
                timeElapsed = 0;

            }
        }
    }
    public override void executeSkill()
    {
        isActive = true;
        this.RaiseEventGlobal<ModifyStatsEvent>(new ModifyStatsEvent
        {
            strengthModifier = Modifiers[0].Strength,
            magicalDefenseModifier = Modifiers[0].MDefense,
            defenseModifier = Modifiers[0].Defense,
            magicalAttackModifier = Modifiers[0].MAttack
        });
        base.executeSkill();
    }
}
