﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SkillSwapEvent : GameEvent
{
    public int skillID;
    public int newSkillID;

    public SkillSwapEvent(int oldID, int newID)
    {
        skillID = oldID;
        newSkillID = newID;
    }
}

public class exitPanelEvent : GameEvent
{
    public bool condition;
}
public class Character_Skills : PauseManager 
{
    public Character_SkillList skillList;
    public List<Skill>       characterSkills = new List<Skill>();
	public Transform         skillLocation;
	public Image             CurrentSkillUI; // UI
    public List<Image>       MainSkillsUI = new List<Image>(); // UI
    public GameObject        MainSkillPanel;

    private Skill            currentSkill;
	private int 		     curSkillID = 0;
	private bool             casting = false;
    private int              selectedSkillID;

	Character_Stats characterStats;
	Animator anim;
    bool show = false;

    void Awake()
    {
        // Add the first 3 skills int the list
        for (int i = 0; i < 3; i++)
        {
            characterSkills.Add(skillList.SkillList[i]);
        }
    }
	void Start()
	{
		characterStats = GetComponent<Character_Stats>();
		anim = GetComponent<Animator>();
        currentSkill = characterSkills[0];
	}
    void OnEnable()
    {
        this.AddEventListenerGlobal<SkillSwapEvent>(swapSkill);
        this.AddEventListener<AddSkillEvent>(addSkill);
        this.AddEventListenerGlobal<exitPanelEvent>(onExitPanelEvent);
    }
    void OnDisable()
    {
        this.RemoveEventListenerGlobal<SkillSwapEvent>(swapSkill);
        this.RemoveEventListenerGlobal<AddSkillEvent>(addSkill);
        this.RemoveEventListenerGlobal<exitPanelEvent>(onExitPanelEvent);
    }
	public override void Move()
	{
		//Cast Skill Animation and the player stops during the casting of the skill
		if (Input.GetKeyDown(KeyCode.Mouse1))
		{
			if (characterStats.getPlayerMana() >= currentSkill.ManaCost)
			{
				casting = true;
				castSkill();	
			}
		}
		else if (Input.GetKeyUp(KeyCode.Mouse1)) // Enable the player to move after the casting of the skill
		{
			casting = false;
		}

		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			curSkillID = 0;
		}
		if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			curSkillID = 1;
		}
		if (Input.GetKeyDown(KeyCode.Alpha3))
		{
			curSkillID = 2;
		}

        if (Input.GetKeyDown(KeyCode.K))
        {
            show = !show;
        }
        if (show)
        {
            MainSkillPanel.SetActive(true);       
        }
        else
        {
            MainSkillPanel.SetActive(false);
        }

        //images
        updateImages();
		damageSkillSelection(curSkillID);
		CurrentSkillUI.sprite = currentSkill.SkillImage;
	}

    void swapSkill(SkillSwapEvent e)
    {
        Skill tempSkill = characterSkills[e.skillID];
        characterSkills[e.skillID] = characterSkills[e.newSkillID];
        characterSkills[e.newSkillID] = tempSkill;
    }
    //updates images
    void updateImages()
    {
        for (int i = 0; i < 3; i++)
        {
            MainSkillsUI[i].sprite = characterSkills[i].SkillImage;
        }
    }

	// Skill Cast System
	void castSkill()
	{
        anim.SetTrigger("Cast");
        if(this.anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1 && !anim.IsInTransition(0))
        {
            currentSkill.executeSkill();
        }     
        this.RaiseEventGlobal<PlayerManaEvent>(new PlayerManaEvent 
        { 
            manaIncrease = -currentSkill.ManaCost
        });
	}
	// Selection of Damage Skill
	void damageSkillSelection(int skillID)
	{
        currentSkill = characterSkills[skillID];
	}

    void addSkill(AddSkillEvent e)
    {
        characterSkills.Add(skillList.SkillList[e.skillID]);
    }
	public bool getCastingSkill()
	{
		return casting;
	}

    public void setShow(bool condition)
    {
        show = condition;
    }
    public void setSelectedSkillID(int id)
    {
        selectedSkillID = id;
    }
    public int getSelectedSkillID()
    {
        return selectedSkillID;
    }

    void onExitPanelEvent(exitPanelEvent e)
    {
        show = e.condition;
    }

}
