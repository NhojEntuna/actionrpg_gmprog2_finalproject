﻿using UnityEngine;
using System.Collections;

public class GoldEvent : GameEvent
{
    public int goldAmount;

    public GoldEvent(int amount)
    {
        goldAmount = amount;
    }

}

public class GetPlayerGoldEvent : GameEvent
{
    public Character_Gold charGold;
}


public class Character_Gold : MonoBehaviour 
{
    public int Gold;
    void OnEnable()
    {
        this.AddEventListenerGlobal<GoldEvent>(addGold);
        this.AddEventListenerGlobal<EnemyDeadEvent>(enemyGoldReward);
        //Character Gold Event
        this.AddEventListenerGlobal<GetPlayerGoldEvent>(OnPlayerGetGoldEvent);
    }

    void OnDisable()
    {
        this.RemoveEventListenerGlobal<GoldEvent>(addGold);
        this.RemoveEventListenerGlobal<EnemyDeadEvent>(enemyGoldReward);
        //Character Gold Event
        this.RemoveEventListenerGlobal<GetPlayerGoldEvent>(OnPlayerGetGoldEvent);
    }

    void addGold(GoldEvent e)
    {
        Gold += e.goldAmount;
    }

    void enemyGoldReward(EnemyDeadEvent e)
    {
        Gold += e.goldReward;
    }

    public int getGold()
    {
        return Gold;
    }

    void OnPlayerGetGoldEvent(GetPlayerGoldEvent e)
    {
        e.charGold = this;
    }
}
