﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System;

[RequireComponent(typeof(CharacterController))]
public class Character_Controller : PauseManager 
{
    // Combat System and Weapon Swap System Members
    public RuntimeAnimatorController bowAnim;
    public RuntimeAnimatorController swordAnim;
    public GameObject                attackCollider;
    public GameObject             Bow;
    public List<GameObject>       Swords = new List<GameObject>();

    // Camera Data Members
    public Transform m_Target;
    public float     m_rotationSpeed;

    // Movement Data Members
	public float m_moveSpeed = 10.0f;
	public float m_jumpSpeed = 5.0f;
	public float m_gravity = 20.0f;

    // UI Data Members
    public Slider m_SprintMeter;
    public float  m_SprintTime;
    public int    m_DesiredSprintSpeed;

    private float   m_defaulMoveSpeed;
    private bool    m_isSprinting = false;
    private Vector3 moveDirection = Vector3.zero;
    private float   elapsedTime;

    CharacterController m_controller;
    Character_Stats player;
    Character_Inventory playerInventory;
    Animator anim;
    bool active = false;

    void OnEnable()
    {
        this.AddEventListenerGlobal<PauseEvent>(OnPause);
    }
    void OnDisable()
    {
        this.RemoveEventListenerGlobal<PauseEvent>(OnPause);
    }
	void Start()
	{
        m_defaulMoveSpeed = m_moveSpeed;
        m_controller = GetComponent<CharacterController>();
        player = GetComponent<Character_Stats>();
        anim = GetComponent<Animator>();
        m_SprintMeter.maxValue = m_SprintTime;
        playerInventory = GetComponent<Character_Inventory>();
	}

	public override void Move()
	{
        float h_Input = Input.GetAxisRaw("Horizontal");
        float v_Input = Input.GetAxisRaw("Vertical");
        
		if (m_controller.isGrounded && player.getPlayerHealth() > 0) 
		{
            if (v_Input != 0)
            {
                anim.SetBool("Running", true);
            }
            else
                anim.SetBool("Running", false);

            if (h_Input != 0)
            {
                anim.SetBool("Running_Side", true);
                anim.SetFloat("H_Axis", h_Input);
            }
            else
                anim.SetBool("Running_Side", false);
            
            // Weapon Swap Algorithm
            if (Input.GetKeyDown(KeyCode.H))
            {
                active = !active;
            }
            if (active)
            {
                var query = (from weapon in playerInventory.EquipmentInventory where weapon.ItemName == "Bow" select weapon).SingleOrDefault();
                if (query != null)
                {
                    playerInventory.curEquipment = query as EquipmentItem;
                    anim.runtimeAnimatorController = bowAnim;
                    Bow.gameObject.SetActive(true);
                    for (int i = 0; i < Swords.Count; i++)
                    {
                        Swords[i].gameObject.SetActive(false);
                    }
                }
                else if (query == null)
                    Debug.Log("No Bow in inventory!");
            }
            else if (!active)
            {
                playerInventory.curEquipment = playerInventory.EquipmentInventory[0];
                anim.runtimeAnimatorController = swordAnim;
                Bow.gameObject.SetActive(false);
                for (int i = 0; i < Swords.Count; i++)
                {
                    Swords[i].gameObject.SetActive(true);
                }
            }

            // Combat system of the player
            if (Input.GetMouseButton(0))
            {
                anim.SetBool("Attack", true);
                playerInventory.curEquipment.executeWeapon();
				attackCollider.GetComponent<BoxCollider>().enabled = true;
            }
			else
			{
				attackCollider.GetComponent<BoxCollider>().enabled = false;
				anim.SetBool("Attack", false);
			}
			moveDirection = new Vector3 (h_Input, 0.0f, v_Input);
			moveDirection.Normalize ();
			moveDirection = transform.TransformDirection(moveDirection);
			moveDirection *= m_moveSpeed;     
		}
       
        if (player.getPlayerHealth() > 0)
        {
            if (Input.GetButtonDown("Jump"))
                moveDirection.y = m_jumpSpeed;
            anim.SetBool("Grounded", m_controller.isGrounded);
        }
		// Rotation of the character with respect to the camera
        if (moveDirection != Vector3.zero)
        {
            Vector3 targetPos = m_Target.position;
            targetPos.y = this.transform.position.y;
            Quaternion targetDir = Quaternion.LookRotation(-(targetPos - transform.position));
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, targetDir, m_rotationSpeed * Time.deltaTime);
        }

        // Sprinting UI and Calculations
        if (Input.GetKey(KeyCode.LeftShift) && moveDirection != Vector3.zero)
        {
            m_isSprinting = true;
              sprint();
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            m_isSprinting = false;
        }
        if (!m_isSprinting)
        {
            regenerateSprint();
        }
        m_SprintMeter.value = m_SprintTime;

        moveDirection.y -= m_gravity * Time.deltaTime;
		// Condition if the player is casting a spell to stop the movement
		bool isCasting = this.gameObject.GetComponent<Character_Skills>().getCastingSkill();
		if(!isCasting)
		m_controller.Move (moveDirection * Time.deltaTime);
	}

    void OnPause(PauseEvent e)
    {
        isPaused = e.paused;
        if (!isPaused)
        {
            anim.StopPlayback();
        }
        else if (isPaused)
            anim.StartPlayback();
    }

    void sprint()
    {
        m_SprintMeter.gameObject.SetActive(true);
        m_moveSpeed = m_DesiredSprintSpeed;
        m_SprintTime -= Time.deltaTime;

        if (m_SprintTime <= 0)
        {
            m_moveSpeed = m_defaulMoveSpeed;
            m_isSprinting = false;
        }   
    }

    void regenerateSprint()
    {
        m_moveSpeed = m_defaulMoveSpeed;
        if (m_SprintTime <= m_SprintMeter.maxValue)
        {
            m_SprintTime += Time.deltaTime;
        }
        // Hide Sprint Meter once it is recharged
        if (m_SprintTime >= m_SprintMeter.maxValue)
        {
            m_SprintMeter.gameObject.SetActive(false);
        }
    }

   

}
