﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AddSkillEvent : GameEvent
{
    public int skillID;
    public AddSkillEvent(int id)
    {
        skillID = id;
    }
}
public class AddExpEvent : GameEvent
{
    public int expReward;
}
public class Character_Level : MonoBehaviour 
{
    public int currentLevel;
    public int nextLevelExp;
    public int curExp;

    public Text levelText;
    public Slider m_ExpUI;

    bool callOnce = true;

    void OnEnable()
    {
        this.AddEventListenerGlobal<AddExpEvent>(addExp);
    }
    void OnDisable()
    {
        this.RemoveEventListenerGlobal<AddExpEvent>(addExp);
    }
    void Update()
    {
        levelText.text = currentLevel.ToString();
        m_ExpUI.maxValue = nextLevelExp;
        m_ExpUI.value = curExp;

        // If the player reachers the NextLevelExp
        if (curExp >= nextLevelExp)
        {
            this.RaiseEvent<LevelUpEvent>(new LevelUpEvent());
            int excessExp = curExp - nextLevelExp;
            curExp = 0;
            curExp += excessExp;
            nextLevelExp += 100;
            currentLevel += 1;
        }

        if (currentLevel == 2 && callOnce)
        {
            this.RaiseEvent<AddSkillEvent>(new AddSkillEvent(3));
            callOnce = false;
        }
    }

    void addExp(AddExpEvent e)
    {
        curExp += e.expReward;
    }
}
