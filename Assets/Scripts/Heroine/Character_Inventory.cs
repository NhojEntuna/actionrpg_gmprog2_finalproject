﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System;
public class AddEquipmentEvent : GameEvent
{
    public EquipmentItem weapon;

    public AddEquipmentEvent(EquipmentItem item)
    {
        weapon = item;
    }
}

public class GetPlayerInventoryEvent : GameEvent
{
    public Character_Inventory charInventory;
}
public class Character_Inventory : PauseManager 
{
    public GameObject m_Invetory;
    // Inventory
    public List<ConsumableItem> ConsumableInventory = new List<ConsumableItem>();
    public List<EquipmentItem>  EquipmentInventory = new List<EquipmentItem>();
	public EquipmentItem  curEquipment; // the current equipment of the player

    private bool show = false;

	void Start()
	{
		curEquipment = EquipmentInventory [0];
	}
    void OnEnable()
    {
        this.AddEventListenerGlobal<AddEquipmentEvent>(addEquipmentItem);
        //Get Player Inventory Event
        this.AddEventListenerGlobal<GetPlayerInventoryEvent>(OnGetPlayerInventory);
    }
    void OnDisable()
    {
        this.RemoveEventListenerGlobal<AddEquipmentEvent>(addEquipmentItem);
        //Get Player Inventory Event
        this.RemoveEventListenerGlobal<GetPlayerInventoryEvent>(OnGetPlayerInventory);
    }
    public override void Move()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            show = !show;
        }

        if (show)
        {
            m_Invetory.SetActive(true);
        }
        else if (!show)
        {
            m_Invetory.SetActive(false);
        }
      
        // If the item is already consumed or used so the quantity will be at 0
   
        for (int i = 0; i < ConsumableInventory.Count(); i++)
        {
            if (ConsumableInventory[i].ItemQuantity == 0)
            {
                ConsumableInventory.Remove(ConsumableInventory[i]);
            }
        }
     }

    public void addConsumableItem(ConsumableItem newItem)
    {
		// Selection of the query if there is a double in the inventory
		var itemQuery = (from consumable in ConsumableInventory where consumable.ItemID == newItem.ItemID select consumable).SingleOrDefault();
		if (itemQuery != null)
		{
			ConsumableItem item = itemQuery as ConsumableItem;
			item.ItemQuantity++;
		}
		else if (itemQuery == null)
		{
            ConsumableInventory.Add(newItem);
			newItem.ItemQuantity++;
			//newItem.transform.position = this.transform.position;
			//newItem.transform.parent = transform;		
		}
        
    }   

    public void removeConsumable(int index)
    {
        Debug.Log("Removes Item: " + ConsumableInventory[index].ItemName);
        if (ConsumableInventory[index].ItemQuantity > 0)
        {
            ConsumableInventory[index].ItemQuantity--;
        }
        else
            ConsumableInventory.RemoveAt(index);
    }
    void addEquipmentItem(AddEquipmentEvent e)
    {
        EquipmentInventory.Add(e.weapon);
    }


    void OnGetPlayerInventory(GetPlayerInventoryEvent e)
    {
        e.charInventory = this;
    }

}

