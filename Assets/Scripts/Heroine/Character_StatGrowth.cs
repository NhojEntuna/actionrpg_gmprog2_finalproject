﻿using UnityEngine;
using System.Collections;

public class LevelUpEvent : GameEvent
{
    public HealSkill healSkill;
}
public class StatusPointEvent : GameEvent
{
    public string statName;

    public StatusPointEvent(string statname)
    {
        statName = statname;
    }
}
public class Character_StatGrowth : MonoBehaviour
{
    private Character_Stats charStats;

    void Start()
    {
        charStats = GetComponent<Character_Stats>();
    }
    void OnEnable()
    {
        this.AddEventListener<LevelUpEvent>(OnLevelUp);
        this.AddEventListenerGlobal<StatusPointEvent>(addStatusPoints);
    }

    void OnLevelUp(LevelUpEvent e)
    {
        charStats.m_MaxHealth += charStats.m_MaxHealth / 2;
        charStats.m_MaxMana += (charStats.m_MaxMana / 2);
        charStats.m_Strength += 5;
        charStats.m_Defense += 5;
        charStats.m_MagicalAttack += 5;
        charStats.m_MagicalDefense += 5;
        charStats.curHealth = charStats.m_MaxHealth;
        charStats.m_Mana = charStats.m_MaxMana;
        charStats.StatPoints += 5;
    }

    void addStatusPoints(StatusPointEvent e)
    {
        if (charStats.StatPoints > 0)
        {
            if (e.statName == "Strength")
            {
                charStats.m_Strength++;
            }
            else if (e.statName == "Defense")
            {
                charStats.m_Defense++;
            }
            else if (e.statName == "MAttack")
            {
                charStats.m_MagicalAttack++;
            }
            else if (e.statName == "Defense")
            {
                charStats.m_MagicalDefense++;
            }
            charStats.StatPoints--;
        }
    }
}
