﻿using UnityEngine;
using System.Collections;

public class ModifyStatsEvent : GameEvent
{
    public int strengthModifier;
    public int defenseModifier;
    public int magicalAttackModifier;
    public int magicalDefenseModifier;
}
public class Character_StatModifier : MonoBehaviour 
{
    void OnEnable()
    {
        this.AddEventListenerGlobal<ModifyStatsEvent>(onBuffEvent);
    }
    void OnDisable()
    {
        this.RemoveEventListenerGlobal<ModifyStatsEvent>(onBuffEvent);
    }

    void onBuffEvent(ModifyStatsEvent e)
    {
        GetPlayerStatsEvent stats = new GetPlayerStatsEvent();
        this.RaiseEventGlobal<GetPlayerStatsEvent>(stats);
        //Modify stas here
        stats.charStats.m_Strength += e.strengthModifier;
        stats.charStats.m_Defense += e.defenseModifier;
        stats.charStats.m_MagicalAttack += e.magicalAttackModifier;
        stats.charStats.m_MagicalDefense += e.magicalDefenseModifier;
    }
}
