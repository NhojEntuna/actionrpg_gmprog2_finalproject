﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class PlayerTakeDamageEvent : GameEvent
{
    public int Damage;
    public PlayerTakeDamageEvent(int damageTaken)
    {
        Damage = damageTaken;
    }
}

public class PlayerHealthEvent : GameEvent
{
    public int healthIncrease;

    public PlayerHealthEvent(int amount)
    {
        healthIncrease = amount;
    }
}

public class PlayerManaEvent : GameEvent
{
    public int manaIncrease;
}

public class PlayerStrengthEvent : GameEvent
{
    public int strengthIncrease;
}

public class PlayerTakeMagicalDamageEvent : GameEvent
{
    public int magicalDamage;
    public PlayerTakeMagicalDamageEvent(int Damage)
    {
        magicalDamage = Damage;
    }
}

public class GetPlayerStatsEvent : GameEvent
{
    public Character_Stats charStats;
}
public class Character_Stats : MonoBehaviour
{
    //Stats
    public int m_MaxHealth;
    public int m_MaxMana;
    public int m_Strength;
    public int m_Defense;
    public int m_MagicalAttack;
    public int m_MagicalDefense;
    public int StatPoints = 5;
    //Private
    public int curHealth;
    public int m_Mana;
    //UI Components
    public Slider m_HealthUI;
    public Text m_HealthText;
    public Text m_MaxHealthText;
    public Slider m_ManaUI;
    public Text m_ManaText;
    public Text m_MaxManaText;
    public GameObject StatusPanel;


    bool isDead;
    bool show = false;
    Animator anim;
    CharacterController controller;
    void Start()
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
        curHealth = m_MaxHealth;
        m_Mana = m_MaxMana;
    }
    void OnEnable()
    {
        this.AddEventListenerGlobal<PlayerTakeDamageEvent>(takeDamage);
        this.AddEventListenerGlobal<PlayerTakeMagicalDamageEvent>(takeMagicalDamage);
        this.AddEventListenerGlobal<PlayerHealthEvent>(addHealthPoints);
        this.AddEventListenerGlobal<PlayerManaEvent>(addManaPoints);
        this.AddEventListenerGlobal<PlayerStrengthEvent>(addStrength);
        //Get Player Stats Event
        this.AddEventListenerGlobal<GetPlayerStatsEvent>(getPlayerStats);
    }
    void OnDisable()
    {
        this.RemoveEventListenerGlobal<PlayerTakeDamageEvent>(takeDamage);
        this.RemoveEventListenerGlobal<PlayerTakeMagicalDamageEvent>(takeMagicalDamage);
        this.RemoveEventListenerGlobal<PlayerHealthEvent>(addHealthPoints);
        this.RemoveEventListenerGlobal<PlayerManaEvent>(addManaPoints);
        this.RemoveEventListenerGlobal<PlayerStrengthEvent>(addStrength);
        //Player Stats Event
        this.RemoveEventListenerGlobal<GetPlayerStatsEvent>(getPlayerStats);
    }
    void Update()
    {
        // Character's HUD 
        m_HealthUI.maxValue = m_MaxHealth;
        m_HealthUI.value = curHealth;
        m_ManaUI.maxValue = m_MaxMana;
        m_ManaUI.value = m_Mana;
        m_MaxHealthText.text = "/ " + m_MaxHealth;
        m_MaxManaText.text = "/ " + m_MaxMana;
        m_HealthText.text = "" + curHealth;
        m_ManaText.text = "" + m_Mana;

        // Status Panel
        if (Input.GetKeyDown(KeyCode.C))
        {
            show = !show;
        }
        if (show)
        {
            int curlevel = this.gameObject.GetComponent<Character_Level>().currentLevel;
            int curExp = this.gameObject.GetComponent<Character_Level>().curExp;
            int nextExp = this.gameObject.GetComponent<Character_Level>().nextLevelExp;
            int nextLevelEXP = nextExp - curExp;

            StatusPanel.SetActive(true);
            this.RaiseEventGlobal<PlayerStatusEvent>(new PlayerStatusEvent(curlevel, m_Strength, m_Defense, m_MagicalAttack, m_MagicalDefense, curExp, nextLevelEXP, StatPoints));
        }
        else
            StatusPanel.SetActive(false);
        if (curHealth <= 0 && !isDead)
        {
            isDead = true;
            death();
        }

        anim.SetBool("isDead", isDead);
    }

    void death()
    {
        curHealth = 0;
        controller.height = 0.2f;
        controller.center = new Vector3(0.0f, 0.1f, 0.0f);
    }
    void takeDamage(PlayerTakeDamageEvent e)
    {
        if (curHealth > 0)
        {
            anim.SetTrigger("Hurt");
            if (m_Defense >= e.Damage)
            {
                int totalDamage = m_Defense - e.Damage;
                curHealth -= totalDamage;
            }
            else
                curHealth -= e.Damage;
        }
        if (curHealth <= 0)
            death();

    }

    void takeMagicalDamage(PlayerTakeMagicalDamageEvent e)
    {
        if (curHealth > 0)
        {
            int totalDamage = e.magicalDamage - m_MagicalDefense;
            if (totalDamage > 0)
            {
                anim.SetTrigger("Hurt");
                curHealth -= totalDamage;
            }
            else // if the totaldamage is less than 0
                curHealth -= 0;
        }
        if (curHealth <= 0)
            death();
    }

    public int getPlayerHealth()
    {
        return curHealth;
    }

    public int getPlayerMana()
    {
        return m_Mana;
    }

    void addManaPoints(PlayerManaEvent e)
    {
        m_Mana += e.manaIncrease;
        if (m_Mana >= m_MaxMana)
        {
            m_Mana = m_MaxMana;
        }
    }

    void addHealthPoints(PlayerHealthEvent e)
    {
        curHealth += e.healthIncrease;
        if (curHealth >= m_MaxHealth)
        {
            curHealth = m_MaxHealth;
        }
    }
    public void useMana(int manacost)
    {
        m_Mana -= manacost;
    }
    void addStrength(PlayerStrengthEvent e)
    {
        m_Strength += e.strengthIncrease;
    }

    //TEMPORARY
    public void addstrength(int amount)
    {
        m_Strength += amount;
    }

    public int getStrength()
    {
        return m_Strength;
    }
    public void addDefense(int amount)
    {
        m_Defense += amount;
    }
    public int getMagicalDefense()
    {
        return m_MagicalDefense;
    }
    public int getMagicalAttack()
    {
        return m_MagicalAttack;
    }

    //Player Stats Event
    void getPlayerStats(GetPlayerStatsEvent e)
    {
        e.charStats = this;
    }
}
