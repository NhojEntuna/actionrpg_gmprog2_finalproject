﻿using UnityEngine;
using System.Collections;
public class Quest_Kill : Quests
{
    public enum KillType
    {
        Specific,
        Any
    }
    public KillType type;
    public string EnemyName;
 
    void OnEnable()
    {
        this.AddEventListenerGlobal<EnemyDeadEvent>(updateQuestProgress);
    }

    void OnDisable()
    {
        this.RemoveEventListenerGlobal<EnemyDeadEvent>(updateQuestProgress);
    }

    public override void Initialize()
    {
        QuestComplete = false;
        base.Initialize();
    }
    void updateQuestProgress(EnemyDeadEvent Event)
    {
        if (Active)
        {
            if (type == KillType.Specific)
            {
                if (Event.enemyName == EnemyName)
                {
                    // Updates the current progress of the player
                    CurrentProgress++;

                    if (CurrentProgress >= RequiredProgress)
                    {
                        // mission accomplished
                        QuestComplete = true;
                    }
                }
            }
            else
                CurrentProgress++;
            if (CurrentProgress >= RequiredProgress)
            {
                QuestComplete = true;
                //AddExpEvent
                this.RaiseEventGlobal<AddExpEvent>(new AddExpEvent
                {
                    expReward = ExpReward
                });
            }
        }      
    }
}
