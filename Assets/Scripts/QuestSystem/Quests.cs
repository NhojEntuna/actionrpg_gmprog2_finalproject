﻿using UnityEngine;
using System.Collections;

public class Quests : MonoBehaviour 
{
    public string QuestName;
    public int    RequiredProgress;
    public int    CurrentProgress;
    public bool   QuestComplete;
    public int    QuestID;
    public bool   Active;
    public int    ExpReward;

    public virtual void Initialize() { }

    void Start()
    {
        Active = false;
        Initialize();
    }
}
