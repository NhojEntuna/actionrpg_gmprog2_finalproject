﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class Quest_Maker : MonoBehaviour 
{
    public List<Quests> QuestList = new List<Quests>();
    
    //UI Components
    public GameObject QuestPanel;
    public Text       questTitle;
    public Text       questProgress;

    // Private
    private Quests currentQuest;
    private int currentQuestID;
    private bool active =  false;

    void Start()
    {
        currentQuestID = 0;
        currentQuest = QuestList[0];
        currentQuest.Active = true;
    }
    void Update()
    {
      // UI 
        if (Input.GetKeyDown(KeyCode.Q))
        {
            active = !active;
        }
        if (active)
        {
            QuestPanel.SetActive(true);
        }
        else
            QuestPanel.SetActive(false);

        questTitle.text = "Quest: " + currentQuest.QuestName;
        questProgress.text = "Progress: " + currentQuest.CurrentProgress.ToString() + " / " + currentQuest.RequiredProgress.ToString();
        
        if (currentQuest.QuestComplete == true)
        {
            if (currentQuestID < QuestList.Count - 1)
            {
                //destroy the previous quest
                QuestList.RemoveAt(currentQuestID);
                currentQuest.CurrentProgress = 0;
                QuestList[currentQuestID].Active = true;
            }
        }

        // Updates the current quest
        currentQuest = QuestList[currentQuestID];
    } 
}
