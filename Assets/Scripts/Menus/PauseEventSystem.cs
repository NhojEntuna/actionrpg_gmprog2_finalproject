﻿using UnityEngine;
using System.Collections;

public class PauseEventSystem : MonoBehaviour 
{
    void OnEnable()
    {
        // Pauses the game
        this.RaiseEventGlobal<PauseEvent>(new PauseEvent
        {
            paused = true
        });
    }

    void OnDisable()
    {
        // unPauses the game
        this.RaiseEventGlobal<PauseEvent>(new PauseEvent
        {
            paused = false
        });
    }
}
