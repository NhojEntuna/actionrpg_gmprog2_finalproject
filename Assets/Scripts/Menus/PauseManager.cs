﻿using UnityEngine;
using System.Collections;

public class PauseEvent : GameEvent
{
    public bool paused;
}

public class PauseManager : MonoBehaviour 
{
    public bool isPaused = false;
    public virtual void Move()
    { }
    public virtual void MoveLateUpdate()
    { }
    void OnEnable()
    {
        this.AddEventListenerGlobal<PauseEvent>(OnPauseEvent);
    }
    void OnDisable()
    {
        this.RemoveEventListenerGlobal<PauseEvent>(OnPauseEvent);
    }

    void Update()
    {
        if (!isPaused)
        {
            Move();
        }      
    }

    void LateUpdate()
    {
        if (!isPaused)
        {
            MoveLateUpdate();
        }
    }

    void OnPauseEvent(PauseEvent e)
    {
        isPaused = e.paused;
    }
}
