﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour 
{
    public static MenuManager instance;
    public GameObject m_PauseMenu;
    public GameObject m_GameOverMenu;
    public GameObject m_Player;

    void Start()
    {
        instance = this;
    }
    void Update()
    {
        if (m_Player.GetComponent<Character_Stats>().getPlayerHealth() >= 0)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                m_PauseMenu.SetActive(true);
            }
        }

        if (m_Player.GetComponent<Character_Stats>().getPlayerHealth() <= 0)
        {
			StartCoroutine(timer(5));
        }
    }

    public void resumeGame()
    {
        m_PauseMenu.SetActive(false);
    }
    public void quitGame(string levelname)
    {
        Application.LoadLevel(levelname);
    }
    public void restartGame()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

	IEnumerator timer(float time)
	{
		yield return new WaitForSeconds(time);
		m_GameOverMenu.SetActive(true);
	}
}
