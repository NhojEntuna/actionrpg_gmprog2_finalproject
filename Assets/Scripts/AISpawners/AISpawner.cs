﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyCurrentEvent : GameEvent
{
    public string enemyName;
    public GameObject monster;
}
public class AISpawner : MonoBehaviour 
{
    public float spawnTimer;
    public int ActiveEnemy;
    public int EnemyCount;
    public List<GameObject> Monsters = new List<GameObject>();
    public List<Transform> spawnPoints = new List<Transform>();

    // List of enemies active on the scene
    public List<GameObject> activeMonsters = new List<GameObject>();
    float timeElapsed = 0.0f;

    void Start()
    {
        for (int i = 0; i < EnemyCount; i++)
        {
            int rand = Random.Range(0, Monsters.Count);
            int randLoc = Random.Range(0, spawnPoints.Count);
            Instantiate(Monsters[rand], spawnPoints[randLoc].position, Quaternion.identity);
        }
    }

    void OnEnable()
    {
        this.AddEventListenerGlobal<EnemyDeadEvent>(removeActiveEnemy);
        this.AddEventListenerGlobal<EnemyCurrentEvent>(addActiveEnemy);
    }
    void OnDisable()
    {
        this.RemoveEventListenerGlobal<EnemyDeadEvent>(removeActiveEnemy);
        this.RemoveEventListenerGlobal<EnemyCurrentEvent>(addActiveEnemy);
    }

    void Update()
    {
        if (activeMonsters.Count <= ActiveEnemy && activeMonsters.Count < EnemyCount)
        {
            timeElapsed += Time.deltaTime;
            if (timeElapsed >= spawnTimer)
            {
                int toSpawn = EnemyCount - activeMonsters.Count;
                for (int i = 0; i < toSpawn; i++)
                {
                    int rand = Random.Range(0, Monsters.Count);
                    int randLoc = Random.Range(0, spawnPoints.Count);
                    Instantiate(Monsters[rand], spawnPoints[randLoc].position, Quaternion.identity);
                }
                timeElapsed = 0;
            }
        }

    }

    void addActiveEnemy(EnemyCurrentEvent e)
    {
        activeMonsters.Add(e.monster);
    }
    void removeActiveEnemy(EnemyDeadEvent e)
    {
        activeMonsters.Remove(e.Monster); 
    }
}
