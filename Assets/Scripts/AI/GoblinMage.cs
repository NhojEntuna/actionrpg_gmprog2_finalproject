﻿using UnityEngine;
using System.Collections;

public class GoblinMage : Enemy
{
	public GameObject attackParticle;
	public Transform  spawnLocation;
    private bool hasReachedLastPoint;

    public override void Initialize()
    {
		callOnce = true;
        hasReachedLastPoint = false;
        curState = EnemyState.Patrol;
        base.Initialize();
    }

    public override void Movement()
    {
        switch (curState)
        {
            case EnemyState.Patrol: updatePatrolState(); break;
            case EnemyState.Idle:   updateIdleState(); break;
            case EnemyState.Chase:  updateChaseState(); break;
            case EnemyState.Dead:   updateDeadState(); break;
            case EnemyState.Attack: updateAttackState(); break;
		    case EnemyState.CoolDown: updateCoolDownState(); break;
        }
		int playerHealth = m_Player.GetComponent<Character_Stats> ().getPlayerHealth ();
        if (m_Health <= 0 && callOnce)
        {
			anim.SetBool("Cast", false);
            curState = EnemyState.Dead;
        }
		// Player is dead
		if (playerHealth <= 0)
		{
			anim.SetBool("Cast", false);	
			curState = EnemyState.Idle;
		}
		// Attack State
		if (Vector3.Distance(this.transform.position, m_PlayerPos.position) <= 10 && playerHealth > 0 && m_Health > 0 && (curState != EnemyState.CoolDown))
		{
			curState = EnemyState.Attack;
		}
        base.Movement();
    }
	protected void updateCoolDownState()
	{
		this.transform.LookAt(m_PlayerPos.position);
		anim.SetBool("Running", false);
		timeElapsed += Time.deltaTime;
		if (timeElapsed >= coolDownTime)
		{
			timeElapsed = 0;
			curState = EnemyState.Attack;
		}
	}
    protected void updateIdleState()
    {
		anim.SetBool("Cast", false);
        anim.SetBool("Running", false);
        timeElapsed += Time.deltaTime;
        if (timeElapsed >= 2.0f)
        {
            if (curPathIndex == m_target.Length - 1)
            {
                hasReachedLastPoint = true;
            }
            else if (curPathIndex == 0)
            {
                hasReachedLastPoint = false;
            }
            if (hasReachedLastPoint)
            {
                curPathIndex--;
            }
            if (!hasReachedLastPoint)
            {
                curPathIndex++;
            }
            curState = EnemyState.Patrol;
            timeElapsed = 0;
        }
    }

    protected void updatePatrolState()
    {
		anim.SetBool("Cast", false);
        anim.SetBool("Running", true);
        m_agent.speed = 3.5f;
        if (Vector3.Distance(this.transform.position, m_target[curPathIndex].position) <= 2)
        {
            curState = EnemyState.Idle;
        }
        // Player in range
        if (Vector3.Distance(this.transform.position, m_PlayerPos.position) <= 15 && m_Health > 0)
        {
            curState = EnemyState.Chase;
        }

        m_agent.SetDestination(m_target[curPathIndex].position);
    }
    protected void updateChaseState()
    {
		this.transform.LookAt(m_PlayerPos.position);
        m_agent.speed = 7.0f;
        m_agent.SetDestination(m_PlayerPos.position);
        if (Vector3.Distance(this.transform.position, m_PlayerPos.position) >= 15)
        {
            curState = EnemyState.Patrol;
        } 
    }
    protected void updateAttackState()
    {
		timeElapsed += Time.deltaTime;
		this.transform.LookAt(m_PlayerPos.position);
		m_agent.SetDestination(this.transform.position);
        anim.SetTrigger("Cast");
		GameObject particle = Instantiate(attackParticle, spawnLocation.position, this.transform.rotation) as GameObject;
		Destroy(particle, 2.0f);

        if (timeElapsed >= .015f)
        {
            timeElapsed = 0;
            curState = EnemyState.CoolDown;

        }
        if (Vector3.Distance(this.transform.position, m_PlayerPos.position) >= 10 && m_Health > 0)
        {
            curState = EnemyState.Chase;
        }
    }
    protected void updateDeadState()
    {
		anim.SetTrigger("Dead");
        anim.SetBool("Cast", false);
        if (callOnce) 
		{
            this.RaiseEventGlobal<EnemyDeadEvent>(new EnemyDeadEvent
            {
                goldReward = m_GoldReward,
                enemyName = m_MonsterType,
                Monster = this.gameObject
            }
           );
            //AddExpEvent
            this.RaiseEventGlobal<AddExpEvent>(new AddExpEvent
            {
                expReward = m_ExpReward
            });
			callOnce = false;
		}
        StartCoroutine(deathTimer(3));
    }

    IEnumerator deathTimer(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(transform.parent.gameObject);
    }
}
