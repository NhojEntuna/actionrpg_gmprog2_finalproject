﻿using UnityEngine;
using System.Collections;

public class GoblinMage_ParticleMovement : MagicalAttack 
{
	public override void Move()
	{
		this.transform.Translate(Vector3.forward * Time.deltaTime * Speed);
	}

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            int damage = Random.Range(damageRange.Min, damageRange.Max);
            this.RaiseEventGlobal<PlayerTakeMagicalDamageEvent>(new PlayerTakeMagicalDamageEvent(damage));
            Destroy(this.gameObject);
        }
    }
}
