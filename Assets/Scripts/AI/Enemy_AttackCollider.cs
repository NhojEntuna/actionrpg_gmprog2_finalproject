﻿using UnityEngine;
using System.Collections;

public class Enemy_AttackCollider : MonoBehaviour 
{
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //Takes the random range damage of the enemy parent
            Range damage = gameObject.GetComponentInParent<Enemy>().m_DamageRange;
            this.RaiseEventGlobal<PlayerTakeDamageEvent>(new PlayerTakeDamageEvent(Random.Range(damage.Min, damage.Max)));
        }
    }
}
