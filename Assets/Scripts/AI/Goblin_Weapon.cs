﻿using UnityEngine;
using System.Collections;

public class Goblin_Weapon : MonoBehaviour 
{
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //NOTE: Add the random range damage
            this.RaiseEventGlobal<PlayerTakeDamageEvent>(new PlayerTakeDamageEvent(10));
        }
    }
}
