﻿using UnityEngine;
using System.Collections;

public class Spider : Enemy 
{
	public override void Initialize()
	{
		curState = EnemyState.Patrol;
		base.Initialize();
	}
	public override void Movement()
	{
		switch (curState)
		{
		case EnemyState.Patrol: updatePatrolState(); break;
		case EnemyState.Idle: updateIdleState(); break;
		case EnemyState.Chase: updateChaseState(); break;
		case EnemyState.Dead: updateDeadState(); break;
		case EnemyState.Attack: updateAttackState(); break;
		case EnemyState.CoolDown: updateCoolDownState(); break;
		}

		int playerHealth = m_Player.GetComponent<Character_Stats> ().getPlayerHealth ();
		if (m_Health <= 0 && callOnce)
		{
			curState = EnemyState.Dead;
		}
		if (playerHealth <= 0)
		{
			curState = EnemyState.Idle;
		}

		//Attack State
		if (Vector3.Distance(this.transform.position, m_PlayerPos.position) <= 3 && playerHealth > 0 && m_Health > 0 && (curState != EnemyState.CoolDown))
		{
			curState = EnemyState.Attack;
		}
        //Enable weapon during the combat
        if (curState == EnemyState.Attack)
        {
            AttackCollider.enabled = true;
        }
        else
            AttackCollider.enabled = false;
		base.Movement();
	}
	protected void updateCoolDownState()
	{
		this.transform.LookAt(m_PlayerPos.position);
		anim.SetBool("Running", false);
		timeElapsed += Time.deltaTime;
		if (timeElapsed >= coolDownTime)
		{
			curState = EnemyState.Attack;
			timeElapsed = 0;
		}
	}
	protected void updateIdleState()
	{
		anim.SetBool("Attack", false);
		anim.SetBool("Running", false);
		timeElapsed += Time.deltaTime;
		if (timeElapsed >= 2.0f)
		{
			curState = EnemyState.Patrol;
			curPathIndex++;
			if (curPathIndex == m_target.Length)
			{
				curPathIndex = 0;
			}
			timeElapsed = 0;
		}
	}
	
	protected void updatePatrolState()
	{
		anim.SetBool("Attack", false);
		anim.SetBool("Running", true);
		m_agent.speed = 3.5f;
		if (Vector3.Distance(this.transform.position, m_target[curPathIndex].position) <= 2)
		{
			curState = EnemyState.Idle;
		}
        if (Vector3.Distance(this.transform.position, m_PlayerPos.position) <= 10 && m_Health > 0)
		{
			curState = EnemyState.Chase;
		}
		
		m_agent.SetDestination(m_target[curPathIndex].position);
	}
	protected void updateChaseState()
	{
		this.transform.LookAt(m_PlayerPos.position);
		anim.SetBool("Attack", false);
		m_agent.speed = 8.0f;
		m_agent.SetDestination(m_PlayerPos.position);
		if (Vector3.Distance(this.transform.position, m_PlayerPos.position) >= 15)
		{
			curState = EnemyState.Patrol; 
		}
	}
	protected void updateAttackState()
	{
		anim.SetBool("Attack", true);
		timeElapsed += Time.deltaTime;
		this.transform.LookAt(m_PlayerPos.position);
		m_agent.SetDestination(this.transform.position);     
		if (timeElapsed >= .5f)
		{
			anim.SetBool("Attack", false);
			curState = EnemyState.CoolDown;
			timeElapsed = 0;
		}
        if (Vector3.Distance(this.transform.position, m_PlayerPos.position) >= 5 && m_Health > 0)
		{
			curState = EnemyState.Chase;
		}
	}
	protected void updateDeadState()
	{
		anim.SetTrigger("Dead");
		anim.SetBool("Attack", false);
		if (callOnce) 
		{
            this.RaiseEventGlobal<EnemyDeadEvent>(new EnemyDeadEvent
            {
                goldReward = m_GoldReward,
                enemyName = m_MonsterType,
                Monster = this.gameObject

            }
           );
            //AddExpEvent
            this.RaiseEventGlobal<AddExpEvent>(new AddExpEvent 
            {
                expReward = m_ExpReward
            });
			callOnce = false;
		}
		StartCoroutine(deathTimer(3));
	}
	
	IEnumerator deathTimer(float time)
	{
		yield return new WaitForSeconds (time);
        Destroy(transform.parent.gameObject);
	}
}