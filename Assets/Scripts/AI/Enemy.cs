﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[System.Serializable]
public class Range
{
    public int Max;
    public int Min;
}
public class EnemyDeadEvent : GameEvent
{
    public int goldReward;
    public string enemyName;
    public GameObject Monster;
}

[RequireComponent(typeof(Rigidbody), typeof(CapsuleCollider), typeof(NavMeshAgent))]
public class Enemy : PauseManager 
{
	public enum EnemyState
	{
		Idle,
		Patrol,
		Chase,
		Attack,
		CoolDown,
		Hurt,
		Dead
	}
    protected Animator anim;
	protected EnemyState  curState;
    public string         m_MonsterType;
    public int            m_Health;
    public Range          m_DamageRange;
    public int            m_ExpReward;
    public int            m_GoldReward;
	public float          coolDownTime;
	public GameObject     damageText;
    public Collider       AttackCollider;
    //Reward List
    public List<GameObject> RandomRewards = new List<GameObject>();
    // UI Components
    public Text enemyName;
    public Slider enemyHealth;

	//Nav Mesh Components
	public Transform[]           m_target;
	protected NavMeshAgent       m_agent;
	protected int                curPathIndex;
    protected GameObject         m_Player;
	protected Transform          m_PlayerPos;
	
	public virtual void Movement() { }
    public virtual void Initialize() { }

	// Timers
	protected float timeElapsed;
	protected bool  callOnce;
	protected bool  hurt;

    // CallOnce for the randomreward
    private bool once;

    void Start()
    {
        this.RaiseEventGlobal<EnemyCurrentEvent>(new EnemyCurrentEvent
        {
            enemyName = m_MonsterType,
            monster = this.gameObject
        });
        enemyName.text = m_MonsterType;
        enemyHealth.maxValue = m_Health;
        enemyHealth.minValue = 0;
        once = true;
		hurt = false;
		callOnce = true;
        anim = GetComponent<Animator>();
        Initialize();
		m_Player = GameObject.FindGameObjectWithTag("Player");
		m_PlayerPos = m_Player.transform;
		curPathIndex = 0;
		m_agent = GetComponent<NavMeshAgent>();

	}
    void OnEnable()
    {
        this.AddEventListenerGlobal<PauseEvent>(OnPause);
    }
    void OnDisable()
    {
        this.RemoveEventListenerGlobal<PauseEvent>(OnPause);
    }
    public override void Move()
    {
        Movement();
        enemyHealth.value = m_Health;
        // Random Reward is Given when the enemy is dead
        if (m_Health <= 0 && once)
        {
            m_agent.Stop();
            int rand1 = Random.Range(0, 5);
            if (rand1 == 2 || rand1 == 4)
            {
                int rand2 = Random.Range(0, RandomRewards.Count - 1);
                Instantiate(RandomRewards[rand2], this.transform.position + new Vector3(0.0f, .5f, 0.0f), RandomRewards[rand2].transform.rotation);
            }
            once = false;
        }
        if (m_Health <= 0)
        {
            m_agent.Stop(true);
            enemyHealth.gameObject.SetActive(false);
            enemyName.gameObject.SetActive(false);
        }
    }
	public void takeDamage(int damage)
	{
        if (m_Health > 0)
        {
            enemyHealth.gameObject.SetActive(true);
            anim.SetTrigger("Hurt");
            GameObject obj = Instantiate(damageText, this.transform.position, this.transform.rotation) as GameObject;
            obj.GetComponentInChildren<Text>().text = damage.ToString();
            m_Health -= damage;
        }
	}
    void OnPause(PauseEvent e)
    {
        isPaused = e.paused;
        if (isPaused)
        {
           anim.StartPlayback();
           m_agent.Stop(true);
        }
        else if (!isPaused)
        {
            anim.StopPlayback();
            m_agent.ResetPath();
        }
        
    }   
}
