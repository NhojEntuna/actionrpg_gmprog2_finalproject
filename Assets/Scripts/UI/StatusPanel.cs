﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerStatusEvent : GameEvent
{
    public int Level;
    public int Strength;
    public int Defense;
    public int MAttack;
    public int MDefense;
    public int CurrentEXP;
    public int NextLevelEXP;
    public int StatusPoints;

    public PlayerStatusEvent(int level, int str, int def, int mAttack, int mDefense, int curExp, int nxtExp, int statPts)
    {
        Level = level;
        Strength = str;
        Defense = def;
        MAttack = mAttack;
        MDefense = mDefense;
        CurrentEXP = curExp;
        NextLevelEXP = nxtExp;
        StatusPoints = statPts;

    }
}

public class StatusPanel : MonoBehaviour 
{
    public Text levelText;
    public Text StrengthText;
    public Text DefenseText;
    public Text MAttackText;
    public Text MDefenseText;
    public Text CurExpText;
    public Text NextLevelText;
    public Text StatPointsText;
    void OnEnable()
    {
        this.AddEventListenerGlobal<PlayerStatusEvent>(updateStats);
    }
    void OnDisable()
    {
        this.RemoveEventListenerGlobal<PlayerStatusEvent>(updateStats);
    }

    void updateStats(PlayerStatusEvent e)
    {
        levelText.text = "Level: " + e.Level.ToString();
        StrengthText.text = "Strength: " + e.Strength.ToString();
        DefenseText.text = "Defense: " + e.Defense.ToString();
        MAttackText.text = "MAttack: " + e.MAttack.ToString();
        MDefenseText.text = "MDefense: " + e.MDefense.ToString();
        CurExpText.text = "CurEXP: " + e.CurrentEXP.ToString();
        NextLevelText.text = "Next Level: " + e.NextLevelEXP.ToString();
        StatPointsText.text = "Status Points: " + e.StatusPoints.ToString();

    }
}
