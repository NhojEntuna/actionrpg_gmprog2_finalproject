﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class SkillPanel : MonoBehaviour 
{
    public List<Skill> Skills = new List<Skill>();
    public List<Image> skillObj = new List<Image>();
    Character_Skills charSkills;
    void Awake()
    {
        charSkills = GameObject.FindGameObjectWithTag("Player").GetComponent<Character_Skills>();
    }

    void OnEnable()
    {
        this.RaiseEventGlobal<PauseEvent>(new PauseEvent
        {
            paused = true
        });
        if (charSkills != null)
        {
            for (int i = 0; i < skillObj.Count; i++)
            {
                Skills.Add(charSkills.characterSkills[i]);
                skillObj[i].sprite = Skills[i].SkillImage;
            }
        }
    }
    void OnDisable()
    {
        Skills.Clear();
    }

    public void exitPanel()
    {
        this.RaiseEventGlobal<PauseEvent>(new PauseEvent
        {
            paused = false
        });
        this.RaiseEventGlobal<exitPanelEvent>(new exitPanelEvent 
        {
            condition = false
        });
    }
   
}
