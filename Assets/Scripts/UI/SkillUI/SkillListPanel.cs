﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System;


public class SkillListPanel : MonoBehaviour 
{
    public List<GameObject> Slots = new List<GameObject>(); // slots
    public List<Skill> skillList = new List<Skill>(); // consumable inventory
    public GameObject SkillSlot;
    private List<GameObject> m_TempSlots = new List<GameObject>(); // temp slots

    float xPos = -145.8f;
    float yPos = 63.5f;
    Character_Skills charSkills;
    int slotAmount = 0;
    bool callOnce = true;
    GameObject slot;
    void Awake()
    {
        charSkills = GameObject.FindGameObjectWithTag("Player").GetComponent<Character_Skills>();  
    }

    void OnEnable()
    {
        this.AddEventListenerGlobal<DestroyCellEvent>(deleteSlot);
        float xPos = -145.8f;
        float yPos = 63.5f;
        slotAmount = 0;

        // Add new cells
        if (charSkills.characterSkills.Count != 0)
        {
            for (int i = 0; i < charSkills.characterSkills.Count; i++)
            {
                slot = Instantiate(SkillSlot) as GameObject;
                slot.GetComponent<SkillList_Slot>().slotNumber = slotAmount;
                Slots.Add(slot); // for slots
                m_TempSlots.Add(slot);
                skillList.Add(new Skill()); // consumable items
                slot.transform.parent = this.gameObject.transform; // this will be a child of this object
                slot.GetComponent<RectTransform>().localPosition = new Vector3(xPos, yPos, 0.0f);
                xPos += 95;
                if (slotAmount == 3)
                {
                    xPos = -145.8f;
                    yPos = yPos - 60;
                }
                slotAmount++;
            }
        }
            
        // adding the character's inventory to the ui
        foreach (Skill skill in charSkills.characterSkills)
        {
            addConsumable(skill.SkillID);
        }
    }
    void OnDisable()
    {
        this.RemoveEventListenerGlobal<DestroyCellEvent>(deleteSlot);
        for (int i = 0; i < m_TempSlots.Count(); i++)
        {
            Destroy(m_TempSlots[i]);
        }

        Slots.Clear();
        skillList.Clear();
    }

    void addConsumable(int id)
    {
        var query = (from skill in charSkills.characterSkills where skill.SkillID == id select skill).SingleOrDefault();
        if (query != null)
        {
            addItemEmptySlot((Skill)query);
        }
    }

    void addItemEmptySlot(Skill item)
    {
        for (int i = 0; i < skillList.Count; i++)
        {
            if (skillList[i].SkillName == null)
            {
                skillList[i] = item;
                break;
            }
        }
    }

    public void exitPanel()
    {
        this.gameObject.SetActive(false);
    }

    void deleteSlot(DestroyCellEvent e)
    {
        Destroy(Slots[e.slotID].gameObject);
    }
}
