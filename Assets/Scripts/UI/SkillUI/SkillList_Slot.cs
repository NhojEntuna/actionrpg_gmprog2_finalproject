﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SkillList_Slot : MonoBehaviour, IPointerDownHandler 
{
    Skill skill;
    Image itemImage;

    SkillListPanel skillInventory; // The Panel of skill inventory
    Character_Skills charSkills;
    public int slotNumber;

    void Start()
    {
        skillInventory = GameObject.FindGameObjectWithTag("SkillPanel").GetComponent<SkillListPanel>();      
        itemImage = gameObject.transform.GetChild(0).GetComponent<Image>();
        charSkills = GameObject.FindGameObjectWithTag("Player").GetComponent<Character_Skills>();
    }

    void Update()
    {
        if (skillInventory.skillList[slotNumber].SkillName != null)
        {
            skill = skillInventory.skillList[slotNumber];
            itemImage.enabled = true;
            itemImage.sprite = skillInventory.skillList[slotNumber].SkillImage;
        }
        else
            itemImage.enabled = false;
    }

    public void OnPointerDown(PointerEventData data)
    {
        if (skillInventory.skillList[slotNumber].SkillName != null)
        {
            int oldSkillID = charSkills.getSelectedSkillID();
            this.RaiseEventGlobal<SkillSwapEvent>(new SkillSwapEvent(oldSkillID, slotNumber));
        }
            
    }
}
