﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class SkillSlot : MonoBehaviour, IPointerDownHandler
{
    public int skillNumber;
    public GameObject SkillListPanel;

    Character_Skills charSkills;
    void Awake()
    {
        charSkills = GameObject.FindGameObjectWithTag("Player").GetComponent<Character_Skills>();
    }
    public void OnPointerDown(PointerEventData data)
    {
        charSkills.setShow(false);
        SkillListPanel.SetActive(true);
        charSkills.setSelectedSkillID(skillNumber);
    }
}
