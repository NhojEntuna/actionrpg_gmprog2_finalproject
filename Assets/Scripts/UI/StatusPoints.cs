﻿using UnityEngine;
using System.Collections;

public class StatusPoints : MonoBehaviour 
{
    public void addStatPoints(string StatName)
    {
        this.RaiseEventGlobal<StatusPointEvent>(new StatusPointEvent(StatName));
    }
}
