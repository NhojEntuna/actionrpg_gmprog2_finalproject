﻿using UnityEngine;
using System.Collections;

public class DamageText : MonoBehaviour 
{
    public float speed;
    public float timer;

    Camera cam;
    void Start()
    {
        cam = Camera.main;
    }
    void Update()
    {
        this.transform.rotation =   cam.transform.rotation;
        this.transform.Translate(Vector3.up * speed * Time.deltaTime);
        Destroy(this.gameObject, timer);
    }

}
